module.exports = {
	title: "Rosecurify",
	url: "https://rosecurify.com/",
	language: "en",
	description: "I am writing about my experiences as a naval navel-gazer.",
	author: {
		name: "Rosecurify",
		email: "securify@rosecurify.com",
		twitter: "https://infosec.exchange/@security",
		url: "https://blog.rosecurify.com/about-me/"
	}
}
